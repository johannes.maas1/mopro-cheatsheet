\documentclass[nobib]{tufte-handout}

\usepackage[utf8]{inputenc}
\usepackage{csquotes}
\usepackage{bm}
\usepackage{ntheorem}
\usepackage{amssymb}

\usepackage{listings}
\lstset{
  basicstyle=\ttfamily
}

\usepackage[
  backend=biber,
  style=authoryear,
  maxbibnames=5,
  ]{biblatex}

\addbibresource{literature.bib}

\newcommand{\sidecite}[2][]{\sidenote{\autocite[#1]{#2}}}
\newcommand{\margincite}[2][]{\marginnote{\autocite[#1]{#2}}}

\DeclareCiteCommand{\shortfullcite}
  {\usebibmacro{prenote}}
  {\usebibmacro{citeindex}%
   \printnames[-1]{labelname}%
   \setunit{\labelnamepunct}
   \printfield[citetitle]{title}%
   \newunit
   \usebibmacro{date}
   \newunit
  }
  {\addsemicolon\space}
  {\usebibmacro{postnote}}

\renewbibmacro*{date}{%
  \printfield{year}
  \iffieldundef{origyear}{%
  }{%
    \setunit*{\addspace}%
    \printtext[brackets]{\printorigdate}%
  }%
}


\newcommand{\link}[2]{\href{#2}{#1}\sidenote{\url{#2}}}

%\geometry{showframe} % display margins for debugging page layout

\usepackage{graphicx} % allow embedded images
  \setkeys{Gin}{width=\linewidth,totalheight=\textheight,keepaspectratio}
  \graphicspath{{graphics/}} % set of paths to search for images
\usepackage{amsmath}  % extended mathematics
\usepackage{booktabs} % book-quality tables
\usepackage{units}    % non-stacked fractions and better unit spacing
\usepackage{multicol} % multiple column layout facilities
\usepackage{lipsum}   % filler text
\usepackage{fancyvrb} % extended verbatim environments
  \fvset{fontsize=\normalsize}% default font size for fancy-verbatim environments

% Standardize command font styles and environments
\newcommand{\doccmd}[1]{\texttt{\textbackslash#1}}% command name -- adds backslash automatically
\newcommand{\docopt}[1]{\ensuremath{\langle}\textrm{\textit{#1}}\ensuremath{\rangle}}% optional command argument
\newcommand{\docarg}[1]{\textrm{\textit{#1}}}% (required) command argument
\newcommand{\docenv}[1]{\textsf{#1}}% environment name
\newcommand{\docpkg}[1]{\texttt{#1}}% package name
\newcommand{\doccls}[1]{\texttt{#1}}% document class name
\newcommand{\docclsopt}[1]{\texttt{#1}}% document class option name
\newenvironment{docspec}{\begin{quote}\noindent}{\end{quote}}% command specification environment


% Matrix, abbreviated mat.
\newcommand{\mat}[1]{\ensuremath{\bm{\mathrm{#1}}}}
\newcommand{\name}[1]{\ensuremath{\mathrm{#1}}}

\theorembodyfont{}
\newtheorem{definition}{Definition}

\newcommand{\secref}[1]{section~\emph{\nameref{#1}}}

\newcommand{\todo}[1]{\textbf{\texttt{\color{red}#1}}}


\begin{document}

\title{Modeling and Verification of Probabilistic Systems}
\author{Johannes Maas}
%\date{28 March 2010} % without \date command, current date is supplied

\maketitle% this prints the handout title, author, and date

\begin{abstract}
  A summary of the lecture \link{\emph{Modeling and Verification of Probabilistic Systems}}{https://moves.rwth-aachen.de/teaching/ws-1819/movep18/} held during the winter semester 2018/19 by \link{Prof. Katoen}{https://moves.rwth-aachen.de/people/katoen/} at RWTH Aachen University.
\end{abstract}

%\printclassoptions

\section{Discrete time Markov chains (DTMCs)}
A discrete-time Markov chain can be characterized both as a time-homogeneous Markov process, and as a transition system.

A Markov process is a stochastic process in which the probabilities only depend on the current state, not the history. Time-homogeneity means that shifting time does not influence the probability, and as a consequence only time differences matter. 

\newcommand{\D}{\mathcal{D}}
As a transition system $(S,P,\iota_{init}, AP, L)$, it has a set of states, a transition probability function,\sidenote{The transition probabilities are somteimtes denoted as a function, but can also be expressed as a matrix.} an initial distribution, a set of atomic properties and a labeling function. A DTMC can thus be represented as a state graph.

A DTMC's paths are infinite, though we also consider the set of their finite prefixes.

\subsection{State residence}
In a DTMC, the probability to remain in the same state for a certain number of transitions obeys a geometric distribution. The geometric distribution is the only discrete probability function that is memoryless. This property means that only \enquote{time} differences matter.

The transient state distribution gives the probability of being in a certain state after exactly $n$ transitions. When expressed as a vector, it is given by
\begin{equation*}
  \Theta_n^\D=\iota_{init}\cdot \mat{P}^n
\end{equation*}

\renewcommand{\vec}[1]{#1}
\renewcommand{\v}{\vec{v}}
The limiting state distribution $\v$ (also called long run distribution) is the probability to be in a certain state after infinitely many transitions. Only if $\mat{P}$ is ergodic, does it have a limiting distribution that is independent from the initial distribution. An ergodic matrix has a limit $\lim_{n\rightarrow\infty} \mat{P}^n$ where each row is identical and represents the limiting state distribution. $\v$ can be determined by the system of linear equations given by
\begin{equation*}
  \underbrace{\v=\v\cdot\mat{P}}_{\iff \v\cdot(\mat{I}-\mat{P})=0} \land \sum_{i\in[1,|\v|]_\mathbb{N}} \v_i=1
\end{equation*}
Note that a DTMC without limiting distribution can have a stationary distribution.\sidenote{A stationary distribution $\vec{\pi}$ fulfills $\vec{\pi}=\vec{\pi}\cdot\mat{P}$, meaning it does not change after taking another transition.} For example, if the DTMC has two unconnected components then the limiting distribution depends on the initial distribution, and the limit will not have identical rows.

\subsection{Reachability}
\renewcommand{\U}{\operatorname{U}}
Reachabilities are expressed as sets. For example, $\lozenge G$ is the set of all paths that at some point visit a state in $G$, $\square G$ all paths that always stay in $G$, $\overline{F}\U G$ all paths that do not visit $F$ until they reach $G$,\sidenote{Note that after visiting $G$, the paths may visit $F$.} $\lozenge\square G$ the paths that at some point will always stay in $G$, and $\square\lozenge G$ the paths that will always return to a state in $G$. The reachabilities can be expressed as each other.
\begin{align*}
  \square G&=\overline{\lozenge\overline{G}}\\
  \lozenge G&=\overline{\emptyset}\U G\\
  \lozenge\square G&=\overline{\square\lozenge\overline{G}}
\end{align*}

\newcommand{\fp}{\hat{\pi}}
To determine their probability, DTMCs need to have a probability space on which these are measurable. Cylinder sets form the basis for that space and are defined for a finite path $\fp=s_0 s_1\ldots s_n$ as the set of all paths that start with $\fp$ as a prefix. The probability of a cylinder set is the probability to start in its first state and transition to each following, formally
\begin{equation*}
  Pr(Cyl(\fp))=\iota_{\fp[0]}\cdot\prod_{i=0}^{|\fp|-2} P(\fp[i],\fp[i+1])
\end{equation*}

With this probability space, reachability probabilities can be expressed as a system of linear equations. First, we need to determine the states that can reach $G$, but are not already in $G$, called $S_?$, e. g. by graph analysis. Then we can calculate the relevant portion of the probability matrix, $\mat{A}$, and the probabilities to reach $G$ in one step for every state in the vector $\vec{b}$. Then the vector $\vec{x}=\mat{A}\cdot \vec{x}+\vec{b}$ describes the reachability probabilities $\vec{x}_s=Pr(s\models \lozenge G)$.\sidenote{An equivalent system of equations is $(\mat{I}-\mat{A})\vec{x}=\vec{b}$.}

Actually, the set $S_?$ can be calculated using the states already in $G$, $S_{=1}$, and the states that cannot reach $G$, $S_{=0}$, as $S_?=S\setminus(S_{=1}\cup S_{=0})$. The sets have the special property that $x_s=Pr(s\models \lozenge G)$ is $1$ for $s\in S_{=1}$ and $0$ for $s\in S_{=0}$. Choosing $S_{=1}$ and $S_{=0}$ as large as possible, which can be done efficiently by graph analysis, minimizes $S_?$ and thus the remaining calculations.

The probability of $\overline{F}\U G$ can be determined iteratively, using $S_?$ as the set of states that can reach $G$ without passing through $F$, and corresponding $\mat{A}$ and $\vec{b}$, by iterating over
\begin{equation*}
  \vec{x}^{(i)}:=\begin{cases}
    0 & i=0\\
    \mat{A}\cdot \vec{x}^{(i-1)} + \vec{b} & i\geq 1
  \end{cases}
\end{equation*}
Then $x:=\lim_{n\rightarrow\infty} x^{(n)}$ gives the probabilities $x_s=Pr(s\models\overline{F}\U G)$. There are different conditions for aborting the iteration. The power method stops if the biggest change in a coordinate of $x^{(i)}$ to $x^{(i+1)}$ is below a threshold $\epsilon$, and it is guaranteed to converge.

Reachability probabilities can be reduced to transient probabilities by making the goal states absorbing.\sidenote{Making a state absorbing means removing all outgoing transitions and replacing them with a self loop, such that the state cannot be left.} This works, because after a goal state has been reached, the following transitions are irrelevant as the condition is already fulfilled. Formally, for a DTMC $\D$, and its version where all states from $G$ are absorbing called $\D[G]$, holds
\begin{equation*}
  Pr_\D(\lozenge^{\leq n}G)=Pr_{\D[G]}(\lozenge^{=n})=\Theta_n^{\D[G]}
\end{equation*}

\subsection{Almost sure reachability}
Almost sure reachabilities in finite DTMCs do not depend on the transition probabilities,\sidenote{For infinite DTMCs this is not the case. Consider the random walk.} and reduces to graph analysis. Formally:
\begin{equation*}
  Pr(s\models \lozenge G)=1 \iff s\in S\setminus(Pre^*(S\setminus Pre^*(G)))
\end{equation*}
The set $S\setminus Pre^*(G)$ contains the states that cannot reach $G$, so $S\setminus(Pre^*(S\setminus Pre^*(G)))$ contains the states that cannot reach states from which on they cannot reach $G$ anymore. This set can be calculated by making all states in $G$ absorbing and then doing a backwards search from $G$ to determine $Pre^*(G)$, and a second backwards search from its complement, $S\setminus(Pre^*(G))$. The time complexity is linear in the size of the DTMC.

Almost sure repeated reachability is equivalent to each reachable BSCC containing at least one state from $G$. It is a consequence of the long-run theorem. Formally:
\begin{equation*}
Pr(s\models \square\lozenge G)=1 \iff \forall T\in BSCC(\D), T\subseteq Post^*(s): T\cap G\neq\emptyset
\end{equation*}
Almost sure persistence is equivalent to all reachable BSCC being contained in $G$, formally
\begin{equation*}
Pr(s\models \lozenge\square G)=1 \iff \forall T\in BSCC(\D), T\subseteq Post^*(s): T\subseteq G
\end{equation*}

Quantitative repeated reachability can, in a finite DTMC, be reduced to reachability probabilities:
\begin{align*}
  Pr(s\models \square\lozenge G)&=Pr(s\models \lozenge (\bigcup_{T\in BSCC(\D),T\cap G\neq\emptyset} T))\\
  Pr(s\models \lozenge\square G)&=Pr(s\models \lozenge (\bigcup_{T\in BSCC(\D),T\subseteq G}T))
\end{align*}

The long-run theorem states that a finite DTMC always ends up in a bottom strongly connected component (BSCC). A BSCC is a strongly connected component that has no outgoing edges, i. e. once reached, it cannot be left. The long-run theorem is proved using the fairness theorem. Formally, almost surely the DTMC $\D$ visits states from some BSCC infinitely often. For a path $\pi$, let $\operatorname{inf}(\pi)$ be the set of all states that are visited infinitely often, and $BSCC(\D)$ the set of $\D$'s BSCCs, then
\begin{equation*}
  Pr\{\pi\in Paths(\D) \mid \operatorname{inf}(\pi)\in BSCC(\D)\}=1
\end{equation*}

The fairness theorem states that if a state is visited infinitely often, then all of its successors are also visited infinitely often. It is called fair, because no successor gets left out by being visited only finitely many times. Formally, the probability to always eventually visit state $t$ is the same as the probability to always eventually visit all of $t$'s successors.

\section{Regular properties}
The trace of a path is the sequence of atomic propositions that results from applying the labeling function to each state in the path.

A linear-time (LT) property $P$ is a subset of infinite traces over the atomic propositions, i. e. $P\subseteq (2^{AP})^\omega$. The probability of such a property $P$ is formally denoted as
\begin{equation*}
  Pr^\D(P)=Pr^\D\{\pi\in Paths(\D) \mid trace(\pi)\in P\}
\end{equation*}

\subsection{Regular safety properties}
A safety property is a linear-time property that can be characterized by a set of bad prefixes. For each trace that is not part of the property, there must exist a finite bad prefix, such that no trace with that prefix is part of the property. It is a regular safety property, if the set of bad prefix constitutes a regular language.

Therefore, a regular safety property can be represented by a deterministic finite-state automaton (DFA). The probability of the safety property can be calculated using that DFA to build a product Markov chain. The product runs the DTMC and the DFA in parallel, by \enquote{feeding} the result of the current DTMC's state's label to the DFA. 

Formally, for DTMC $\D:=(S,\mat{P},\iota_{init},AP,L)$ and DFA $\mathcal{A}:=(Q,2^{AP},\delta,q_0,F)$, the product is $D\otimes \mathcal{A}:=(S\times Q, \mat{P}',\iota_{init}',\{accept\},L')$. $\mat{P}'$ is given by $\mat{P}'(\langle s,q\rangle, \langle s', \delta(q,L(s))\rangle)=\mat{P}(s,s')$, i. e. \enquote{feeding} the label $L(s)$ to the DFA and keeping the transition probability. The initial distribution $\iota_{init}'$ is constructed as $\iota_{init}'(\langle s,\delta(q_0,L(s))\rangle)=\iota_{init}(s)$, i. e. by \enquote{feeding} each of the DTMC's initial states' labels to the DFA's initial state. The labeling mimics the DFA's acceptance by setting
\begin{equation*}
  L'(\langle s,q\rangle)=
    \begin{cases}
      \{accept\} & q\in F\\ \emptyset & q\notin F
    \end{cases}
\end{equation*}

The probability that a state satisfies the property is the reachability probability of the product's final states, which can be determined with a system of linear equations.\sidenote{For qualitative probabilities, a graph analysis suffices.}

\subsection{$\omega$-regular properties}
An $\omega$-regular expression is composed of finite prefixes followed by infinite repetitions of of finite words, i. e. $x_1\cdot y_1^\omega+x_2\cdot y_2^\omega+\ldots$. Note that the repeated part cannot be empty. An LT property is $\omega$-regular, if there it can be characterized using an $\omega$-regular expression.

Deterministic Büchi automata (DBAs) capture a subset of $\omega$-regular properties. Since it considers infinite runs, the acceptance condition differs from DFAs. A DBA accepts a run, if the run visits its final states infinitely often. The language of any DBA is $\omega$-regular, but not all $\omega$-regular languages can be described as DBAs.\sidenote{DBAs recognize the limit language of a regular language.}

Deterministic Rabin automata (DRAs) are $\omega$-regular. Their final states are pairs of sets, and a run is accepted, if for some final state pair $(L,K)$ the set $L$ is visited only finitely often, and $K$ is visited infinitely often.\sidenote{A DBA is a DRA with a final state pair of the form $\{(\emptyset, F)\}$.}

The probability of satisfying an $\omega$-regular property can be reduced to reachability probabilities in a product DTMC. That product of a DTMC and a DRA is similar to that between a DTMC and a DFA, but the acceptance condition must be adapted to fit that of DRAs. The difference is that here the atomic propositions are the DRA's set of final state pairs $\{(L_1,K_1),\ldots\}$ but flattened, thus $AP=\{L_1,K_1,L_2,K_2,\ldots\}$. The labeling function tracks which of these are fulfilled by each state, thus
\begin{equation*}
  L'(\langle s,q\rangle)=\{M_i\in AP \mid q\in M_i \}
\end{equation*}
An accepting BSCC is a BSCC that contains no states that are labeled with an $L_i$, but that does contain at least a state labeled with some $K_i$. With the long-run theorem, it follows that the DRA's acceptance will be satisfied. This makes it possible to reduce a $\omega$-regular property's probability to the reachability probabilities of accepting BSCCs in the product.

Linear temporal logic (LTL) includes operators such as $\bigcirc\phi$ (next) and $\phi\U\psi$ (until). LTL is $\omega$-regular and can thus be expressed with DRAs.

\section{Probabilistic computation tree logic (PCTL)}
\renewcommand{\P}{\mathbb{P}}
Properties over DTMCs can be expressed with PCTL. Its special operator is $\P_J(\phi)$ which tests whether the path formula $\phi$ has a probability in the interval $J$. Path formulae have the next operator $\bigcirc\Phi$, the until operator $\Phi_1\U\Phi_2$, and the bounded until operator $\Phi_1\U^{\leq n}\Phi_2$ for state formulae $\Phi$. Other reachability operators such as $\lozenge\Phi$ can be derived from these. State formulae also contain negation and conjunction, and can also test for the state being associated with a label $a$.

The semantics of $s\models\P_J(\phi)$ is to hold when all paths starting in $s$ satisfy $\phi$ with a probability in interval $J$, formally\sidenote{$Paths(s)$ is the set of all paths that start with $s$.}
\begin{equation*}
  s\models\P_J(\phi) \iff Pr\{\pi\in Paths(s) \mid \pi\models\phi\}\in J
\end{equation*}

PCTL model checking of $s\models\Phi$ is implemented as a recursive, bottom-up computation of the satisfaction sets by breaking $\Phi$ into its parse tree. In the end, a member check of $s$ in $\Phi$'s satisfaction set gives the answer. The calculation for the satisfaction set of the $\P$-operator is defined by structural induction. For the next operator, the probability to transition to a satisfying state is summed up, formally with characterization vector
\begin{equation*}
  \vec{b}_\Phi:=\left(\begin{cases}
    1 & s\in Sat(\Phi)\\
    0 & s\notin Sat(\Phi)
  \end{cases}
  \right)_{s\in S}
\end{equation*}
holds
\begin{equation*}
  (Pr(s\models\bigcirc\Phi))_{s\in S}=\mat{P}\vec{b}_\Phi
\end{equation*}

For the bounded until operator $\Phi\U^{\leq n}\Psi$, a similar technique to reachability probabilities is used, where the states satisfying it already are grouped as $S_{=1}:=Sat(\Psi)$, and the states that cannot satisfy the formula as $S_{=0}:=S\setminus (Sat(\Phi)\cup Sat(\Psi))$.\sidenote{As with reachability probabilities, $S_{=0}=Sat(\P_{=0}(\Phi\U^{\leq n}\Psi))$ and $S_{=1}$ similarly characterized can be chosen maximally big by graph analysis, to minimize the work with $S_?$.} The remaining states have to be checked and are denoted $S_?:=S\setminus(S_{=0}\cup S_{=1})$. The probability can be determined iteratively using the characteristic vector $\vec{b}_\Psi$ and iteratively multiplying it from the left with $P_{\Phi,\Psi}$, the transition probability matrix of $\D[S_{=0}\cup S_{=1}]$.\sidenote{Recall that $\D[S_{=0}\cup S_{=1}]$ is the DTMC $\D$ with all states in $S_{=0}\cup S_{=1}$ made absorbing.}

The probability for the unbounded until operator $\Phi\U\Psi$ can be determined by graph analysis for $S_{=0}=Sat(\P_{=0}(\Phi\U\Psi))$ and $S_{=1}=Sat(\P_{=1}(\Phi\U\Psi))$. Then for $S_?:=S\setminus(S_{=0}\cup S_{=1})$ the probabilities can be determined through a system of linear equations as with reachability with the relevant part of the probability matrix, $\mat{A}$, and the probability to reach a state in $S_{=1}$ in one step, $\vec{b}$, of the form $(\mat{I}-\mat{A})\vec{x}=\vec{b}$.

The time complexity of PCTL model checking is polynomial in the size of $\D$, and linear in the biggest $n$ in a bounded until and the size of the formula. Stopping the convergence when two iterations differ less than a threshold is unsound.\sidenote{When the convergence is slow, the iteration will be stopped too early. There is no guarantee to be close to the actual value with unsound iteration.} This can be handled by lower and upper bounds. The error is $Pr(\lozenge G)-Pr(\lozenge^{\leq k}G)$, i. e. the probability to reach $G$ in more than $k$ steps. That is less than $Pr(\square^{\leq k}S_?)\cdot\max_{s\in S_?}Pr(\lozenge G)$, but more than $Pr(\square^{\leq k}S_?)\cdot\min_{s\in S_?}Pr(\lozenge G)$.

\subsection{Qualitative PCTL}
Removing bounded until and only allowing the bounds $>1$ and $=0$\sidenote{The bounds $=1$ and $>0$ can be derived from these.} yields qualitative PCTL. Computation tree logic (CTL, without P) has the $\exists$- and $\forall$-operators instead of $\P$. A PCTL formula $\Phi$ is equivalent to a CTL formula $\Psi$, if $Sat(\Phi)=Sat(\Psi)$ for all DTMCs. Example equivalences include $\P_{>0}(\lozenge a)\equiv \exists\lozenge a$ and $\P_{=1}(\square a)\equiv \forall\square a$, but note that $\P_{>0}(\square a)\not\equiv\exists\square a$ and $\P_{=1}(\lozenge a)\not\equiv\forall\lozenge a$.

Qualitative PCTL and CTL have incomparable expressive power, i. e. some PCTL formulae are not expressible in CTL and vice versa.\sidenote{This only holds in general, as for finite DTMCs $\P_{=1}(\lozenge a)\equiv \forall((\exists\forall a)\operatorname{W}a)$ where $\Phi\operatorname{W}\Psi:=(\Phi\U\Psi)\lor \square\Phi$ is the weak until operator.} Almost sure reachability is not expressible in CTL, i. e. there is no equivalent for $\P_{=1}(\lozenge a)$ or $\P_{>0}(\square a)$. On the other hand, there is no qualitative PCTL equivalent for $\forall\lozenge a$ or $\exists\square a$. The difference is that CTL only characterizes the underlying graph, while qualitative PCTL takes the probabilities into account, but cannot ignore them.

However, in finite DTMCs the two logics can be made equally expressive by requiring fairness. In a fair path, when a state is visited infinitely often, all of its successors are also visited infinitely often. Fair CTL only considers fair paths for $\exists$ and $\forall$, i. e. paths that do not satisfy the operator, but are unfair, are ignored. Qualitative PCTL and fair CTL are equally expressive for finite Markov chains.

Almost-sure repeated reachability and similar probabilities are definable in qualitative PCTL by nesting $\P$-operators, e. g. $s\models \P_{=1}(\square\P_{=1}(\lozenge G)) \iff Pr\{\pi\in Paths(s) \mid \pi\models\square\lozenge G\}=1$.

\section{Bisimulation}
A strong bisimulation is a relation\sidenote{Not every strong bisimulation is an equivalence relation, because it might not be transitive.} between two labeled transition systems (LTS), such as DTMCs. The relation associates states from the two LTSs in a way that the labels correspond and that the transitions always end up in equivalent states.\sidenote{There are two ways to define strong bisimulation.} Two LTSs are strongly bisimilar, if there exists a bisimulation. The quotient of an LTS is the reduction to its equivalence classes under bisimulation.

Probabilistic bisimulation is an equivalence relation\sidenote{In contrast to strong bisimulation, probabilistic bisimulation is always an equivalence relation.} requires that two bisimilar states have the same probability to move to each equivalence class. Even though there might be different amounts of transitions, the sums of those ending up in the same equivalence class should be equal. Two DTMCs are bisimilar, if each equivalence class has equal initial probability in each DTMC.

Bisimulation preserves PCTL in the sense that two bisimilar states are always equivalent under PCTL, i. e. there is no PCTL formula that can distinguish the states. There is an extension, PCTL$^*$, and a reduced form PCTL$^-$ that have the same property. PCTL$^*$ allows mixing of state and path formulae, while PCTL$^-$ only has disjunction, conjunction, and a upper-bounded $\P_{\leq p}$ operator. Probabilistic bisimulation coincides with the notion of lumpability, as it is the coarsest possible lumpable partition.

The strategy to build a DTMC's quotient under probabilistic bisimulation is to put all nodes with the same label into an equivalence class. Then all outgoing transitions from supposedly equivalent states are checked to land in the same equivalence class. If there is a case where a state has a transition to an equivalence class that other supposedly equivalent states lack, then this equivalence class must be divided into two. This is repeated until bisimilarity is achieved.

\section{Markov decision processes (MDPs)}
A Markov decision process represents a class of DTMCs, therefore it allows for multiple outgoing distributions for a single state. Each of those distribution is identified by an action. The choice between available actions is deliberately unspecified and thus non-deterministic. A DTMC is an MDP that only has one action associated with each state. Paths through a MDP are a sequence of states interjected with the actions that were taken to transition between states.

Policies can resolve the non-determinism by choosing an action depending on the history of states visited. An MDP together with a policy induces a normal, albeit always infinite Markov chain. Each state in the induced DTMC represents the current state in the MDP and its history up to that point, which can be represented with the alphabet containing all non-empty paths over the MDP's states, e. g. $\{s_0,s_1,\ldots,s_n,s_0s_1,s_0s_2,\ldots\}$. Only together with a policy does it make sense to reason about probability in an MDP, and the MDP's probabilities under a certain policy are reduced to the probabilities in the induced DTMC.

Even though generally policies depend on the full history, positional policies are practical and simpler, because they only depend on the current state. Finite-memory policies are represented as a deterministic finite automaton, thus positional policies are also finite-memory with a DFA containing only a single mode. There are more types of policies, notably randomized policies, which have different levels of power.

\subsection{Reachability}
Since the probabilities in an MDP depend on the chosen policy, the interesting probabilities are the maximal and minimal reachability probabilities for states with respect to all possible policies. They can be determined by a system of linear equations. The maximal and minimal reachability probability can always be expressed as a positional policy.

There are different approaches to actually calculating the reachability probability $Pr^{max}(\lozenge G)$. Value iteration uses successive approximation. First, the states in $G$ are set to $1$, and the states that cannot reach $G$ are set to $0$. Then (for maximal reachability) all remaining states are initially set to $0$ and then iterated by choosing the maximizing action for each state.\sidenote{For minimal reachability, the states are initially set to $0$ and iterated by choosing the minimizing actions.}

Reachability can also be expressed as a linear program with polynomial complexity. Another approach is policy iteration, where an initial positional policy randomly associates the states that can reach $G$, but are not already in $G$ with some action. Then each state can be iterated by choosing the action that improves the probability, until no improvement can be made. In a finite MDP, this takes a finite amount of time, since there are only finitely many states and actions to try out.

\subsection{PCTL}
PCTL can be applied to MDPs, where $\P_J(\Phi)$ requires all policies to fulfill $\Phi$ with a probability in $J$. If two formulae are equivalent under MDP-PCTL, then they are also equivalent under MC-PCTL, since DTMCs are also MDPs. The inverse does not hold.

Model checking of MDP- and MC-PCTL formulae are similar, but the satisfaction sets for the $\P$-operator for MDPs are defined with maximums and minimums, to determine the worst case with regards to all policies.

There is an algorithm to compute qualitative reachability by graph transformations.

Fairness can be defined for MDPs. A policy is fair, if the unfair paths have $0$ probability. In an MDP, fairness is realizable if there exists a fair policy. Fairness is irrelevant for maximal reachabilities, but relevant for minimal reachability. Together with strongly fair policies, minimal reachability is reducible to maximal reachability.

\section{Continuous-time Markov chains (CTMCs)}
In a continuous-time Markov chain, state residences are modeled to be exponentially distributed. A CTMC therefore has a rate function, $r$, that assigns each state its exit rate. The rate function is sometimes also combined with the probability matrix into the transition rate matrix, $\mat{R}(s,s'):=\mat{P}(s,s')\cdot r(s)$.

CTMCs distinguish between the probability that a transition is enabled, and the probability that a transition is actually taken. A transition $s\rightarrow s'$ is enabled in time interval $t$ with probability $1-exp(-\mat{R}(s,s')\cdot t)$. The probability to move away from $s$ in time interval $t$ is $1-exp(-r(s)\cdot t)$. The probability to move from $s$ to $s'$ in time interval $t$ is the probability to move away at all weighted with the transition rate, $\frac{\mat{R}(s,s')}{r(s)}(1-exp(-r(s)\cdot t))$.

The underlying exponential distribution is the continuous counterpart to the geometric distribution underlying DTMCs. It is the only memoryless continuous distribution. Additionally, it is closed under minimum, formally $Pr\{\min(X,Y)\leq t\}=1-exp(-(r_X+r_Y)\cdot t)$, and the probability of \enquote{winning the race} can be expressed solely with the competing rates, formally $Pr\{X\leq Y\}=\frac{r_X}{r_X+r_Y}$.

\subsection{Transient probabilities}
The probability to be in a state at a specific point in time is given by a system of linear differential equations, which can be calculated using Taylor-Maclaurin expansion. However, this is numerically instable.

Numerical stability can be achieved by uniformizing the CTMC beforehand. Uniformization entails setting all states' rates to the same value. To ensure that the rates are only increased, they are typically set to the maximum original rate. The excess rate can be compensated by weighted self loops. After uniformization, the calculations of the transient probabilities are characterized by a Poisson distribution, and the error can be bounded.

A uniformized CTMC is no longer strongly bisimilar to the original one, because of the introduced self loops. Therefore, the notion of weak bisimilarity ignores the probability to stay in the same equivalence class and only requires that bisimilar states move to different equivalence classes with the same probability.

\subsection{Timed probabilities}
Timed paths need to contain the instant at which the transition occurred, therefore they are sequences of states interjected with the transition instants. As a consequence, the probability space of a CTMC is defined over timed cylinder paths, that parameterized by a prefix sequence of states interjected with intervals. A path $\pi=s_0t_1s_1t_2s_2\ldots t_ns_n$ is in the timed cylinder set of $s_0T_1s_1T_2s_2\ldots T_ns_n$ if it visits every state, $\pi[i]=s_i$, at the right time $t_i\in T_i$.

A problematic type of timed paths are Zeno\sidenote{Zeno is the inventor of the paradox with Achilles and the tortoise.} paths, which are paths that take infinitely many transitions, but in a finite amount of time. Fortunately, almost surely Zeno paths do not occur in CTMCs, i. e. the probability mass of Zeno paths in a CTMC is $0$.

Untimed reachability probabilities in a finite CTMC  can be reduced to the embedded DTMC.

Timed reachability probabilities can be solved using a system of Volterra integral equations. Due to its complexity, it is reduced to transient probabilities in the CTMC where all goal states have been made absorbing. For constrained timed reachability, additionally the set of states to avoid needs to be made absorbing. Weak and strong bisimilarity preserve transient probabilities and thus timed reachability.

\section{Probabilistic automata (PAs)}
It is desirable to compose a complex automaton out of smaller pieces. Formally, a PA is defined as $(S,s_0,A,\rightarrow)$, where $S$ is the set of states, $s_0\in S$ is the initial state, $A$ is a set of actions, and $\rightarrow\subseteq S\times A\times Dist(S)$ is a set of transitions. During parallel composition, two PAs may synchronize on actions. In contrast to MDPs, PAs may have multiple identical actions outgoing from the same state, which can lead to non-determinism.

For parallel composition, a partial function $\delta: A_1\times A_2\rightarrow A$ controls which actions may synchronize. For all $A_1$ and $A_2$ in the domain of $\delta$, the composite takes transitions $s_1\xrightarrow{A_1}s_2$ and $s_1'\xrightarrow{A_2}s_2'$ simultaneously, i. e. includes $(s_1,s_1')\xrightarrow{A}(s_2,s_2')$. Note however that it also allows to take just $A_1$ or just $A_2$ as additional transitions.

The congruence theorem allows for optimization during composition, because parts of a composition may be replaced by compositionally bisimilar parts, while keeping bisimilarity for the composite. Formally, $P\sim_p Q$ implies for all PAs $R$ that $P||R\sim_p Q||R$ and $R||P\sim_p R||Q$. Therefore, during composition, intermediary PAs may be minimized with respect to compositional bisimilarity.

Bisimulation can be lifted to PAs. But because PAs allow multiple actions, convex combinations of transitions with the same action are not considered bisimilar with previous notions, giving rise to the notion of combined bisimulation.

There is a syntax to describe PAs textually. The expansion law shows how to reduce parallel composition to transitions, making it essentially syntactic sugar.

\section{Markov automata (MAs)}
Parallel composition of CTMCs would need a mechanism to synchronize, but actions are not part of CTMCs. Additionally, actions cannot simply be added to delayed transitions, because their composition would require the maximum of the delays' exponential distributions, which itself is no longer an exponential distribution.

Instead, a Markov automaton has two types of transitions, ones with delays and ones with actions. The special action $\tau$ is internal, meaning it cannot be used to synchronize in parallel composition. Hiding actions means making them internal, i. e. $\tau$. The maximal progress assumptions declares that $\tau$-actions always fire before any competing delay transitions.

Parallel composition is synchronous on the actions, meaning they can only be taken on both parts together. It is also backwards compatible to composition on PAs.

Previous notions of bisimulation differentiate between delay and action transitions. However, it makes sense to combine both types into one, by transforming delay transitions into action transitions that are labeled with the exit rate, and whose distribution is proportional to the transition rates. I. e., transitions $s_1\xrightarrow{r_1}s_2$ and $s_1\xrightarrow{r_2}s_3$ will be transformed to a transition $(r_1+r_2).([\frac{r_1}{r_1+r_2}]s_2 + [\frac{r_2}{r_1+r_2}]s_3)$. Such bisimulation is congruent with respect to parallel composition and hiding, making it possible to intermittently minimize large compositions.

The expected time to reach states depends on the policy with which competing actions are determined. Therefore, in MAs it is mostly useful to consider bounds. Minimal expected time to reach a state under the most demonic policy can be calculated as the fixpoint of the Bellman operator. That minimal time can always be achieved by a positional policy. As a corollary, it can be reduced to the stochastic shortest path problem.

Petri nets reduce to MAs, by encoding the distribution of tokens into the states and mimicking the transitions.


\clearpage
\printbibliography


\end{document}
